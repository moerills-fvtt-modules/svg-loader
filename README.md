# SVG Loader
## Important Information
I don't actively maintain this module anymore. (Since quite a while already) This is also the reason why it is not listed under the official FVTT module listing!  
If any serious issues arise, which are fixable in a very small amount of time i *may* return and fix it.  
Saying all this, this module is, judging from the many many months it didn't need a real update, quite stable and won't break to easily.  

Thanks to the people from the community who created the last  two fixes! If anyone is interested in maintaining this module, feel free to do so! You can either fork it or talk with me and i can give you the rights to work on it for this repository.

## Installation
* Download the [Zip file](https://gitlab.com/moerills-fvtt-modules/svg-loader/-/archive/master/svg-loader-master.zip).
* Extract the zip file into your ``public/modules`` folder.
* Restart your FVTT server.
* Activate the  module under ``Manage Modules``.

## Usage
- Right click on the scene you want to import the data into and click on ``Import SVG Data``.
- Click on ``Choose File`` to choose your SVG file.
- Click on ``Import`` and wait until ``SUCCESS!`` is shown at the top.  
**Do not switch the scene until ``SUCCESS!`` is shown at the top!**  

![](img/svg_loader.gif)
Huge shoutout to *Forgotten Adventures* for his token-art used in the video! Check out his other work at [his Patreon](https://www.patreon.com/forgottenadventures/overview)

## Compatibility
- Tested on FVTT 0.3.0.  
- Implemented for DungeonFog SVG files, version ``1``.
- Also supporting regular SVG files, with the following format:
    - Paths are interpreted as vision blocking walls
    - Lines as doors
    - Circles as local lights

## Contribution
If you like my work and wand to support me, feel free to leave a tip at my [![paypal](https://img.shields.io/badge/-Paypal-blue.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FYZ294SP2JBGS&source=url).

## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">SVG Loader - a module for Foundry VTT -</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/moerills-fvtt-modules/adnd5e" property="cc:attributionName" rel="cc:attributionURL">Mörill</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).